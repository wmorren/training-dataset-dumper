#include "JetWriters/getJetLinkWriter.h"
#include "JetWriters/IJetLinkWriter.h"

#include "JetWriters/JetElement.h"
#include "JetWriters/AssociationConfig.h"
#include "JetWriters/JetLinkWriter.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODBase/IParticle.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODEgamma/Electron.h"
#include "xAODPFlow/FlowElement.h"
#include "xAODJet/Jet.h"

namespace {

  namespace jw = jetwriters;

  // define functions to retrieve cluster and track objects from
  // particle flow objects
  auto getFlowAssociations() {

    using Type = xAOD::FlowElement;
    using ElementType = JetElement<Type>;
    using PartLinks = std::vector<ElementLink<xAOD::IParticleContainer>>;

    // track getter
    auto track = [](const ElementType& el) -> const xAOD::TrackParticle* {
      if (!el.constituent) throw std::runtime_error("missing flow object");
      size_t n_charged = el.constituent->nChargedObjects();
      // see AFT-619: neutral UFO constituents have n_charged == 1
      if (not el.constituent->isCharged()) return nullptr;
      if (n_charged != 1) throw std::runtime_error(
        "n charged > 1, found " + std::to_string(n_charged));
      const auto* obj = el.constituent->chargedObject(0);
      if (!obj) throw std::runtime_error("charged object missing");
      const auto* track = dynamic_cast<const xAOD::TrackParticle*>(obj);
      if (!track) throw std::runtime_error("can't cast to track particle");
      return track;
    };

    // cluster getter
    auto cluster = [](const ElementType& el) -> const xAOD::CaloCluster* {
      if (!el.constituent) throw std::runtime_error("missing flow object");
      size_t n_clusters = el.constituent->nOtherObjects();
      if (n_clusters == 0) return nullptr;
      const xAOD::IParticle* obj = nullptr;
      if (n_clusters == 1) {
        obj = el.constituent->otherObject(0);
      } else {
        // if we have a few clusters take the one with the highest weight
        auto ops = el.constituent->otherObjectsAndWeights();
        obj = std::max_element(
          ops.begin(), ops.end(),
          [](const auto& x, const auto& y) { return x.second < y.second; }
          )->first;
      }
      if (!obj) { // very rare: handle slimmed negative energy clusters
        return nullptr;
      }
      const auto* cluster = dynamic_cast<const xAOD::CaloCluster*>(obj);
      if (!cluster) throw std::runtime_error("can't cast to calo cluster");
      return cluster;
    };

    // build the association tuple
    return std::tuple{
      jw::getAssociationConfig(
        [track] (const ElementType& e) -> const xAOD::TrackParticle* {
          return track(e);
        },"track"),
      jw::getAssociationConfig(
        [cluster](const ElementType& e) -> const xAOD::CaloCluster* {
          return cluster(e);
        }, "cluster")
    };
  }


  // define functions to retrieve cluster and track objects from electron
  auto getElectronAssociations() {

    using Type = xAOD::Electron;
    using ElementType = JetElement<Type>;
    using PartLinks = std::vector<ElementLink<xAOD::IParticleContainer>>;

    // cluster getter
    auto cluster = [](const ElementType& el) -> const xAOD::CaloCluster* {
      if (!el.constituent) throw std::runtime_error("missing object");
      const auto* obj = el.constituent->caloCluster();
      if (!obj) throw std::runtime_error("neutral object missing");
      const auto* cluster = dynamic_cast<const xAOD::CaloCluster*>(obj);
      if (!cluster) throw std::runtime_error("can't cast to calo cluster");
      return cluster;
    };

    // track getter
    auto track = [](const ElementType& el) -> const xAOD::TrackParticle* {
      if (!el.constituent) throw std::runtime_error("missing object");
      const auto* obj = el.constituent->trackParticle();
      if (!obj) throw std::runtime_error("charged object missing");
      const auto* track = dynamic_cast<const xAOD::TrackParticle*>(obj);
      if (!track) throw std::runtime_error("can't cast to track particle");
      return track;
    };

    // build the association tuple
    return std::tuple{
      jw::getAssociationConfig(
        [cluster] (const ElementType& e) -> const xAOD::CaloCluster* {
          return cluster(e);
        }, "cluster"),
      jw::getAssociationConfig(
        [track](const ElementType& e) -> const xAOD::TrackParticle* {
          return track(e);
        }, "track")
    };
  }

}


std::unique_ptr<IJetLinkWriter> getJetLinkWriter(
  H5::Group& group,
  const JetLinkWriterConfig& cfg) {
  if (cfg.constituents.size == 0) {
    return nullptr;
  }
  using Tp = JetLinkWriterConfig::Type;
  using F = xAOD::FlowElement;
  using FA = decltype(getFlowAssociations());
  using E = xAOD::Electron;
  using EA = decltype(getElectronAssociations());
  using I = xAOD::IParticle;
  using IC = xAOD::IParticleContainer;

  switch (cfg.type) {
  case Tp::UNKNOWN: throw std::logic_error("linked particle of type UNKNOWN");
  case Tp::IParticle: return std::make_unique<JetLinkWriter<I,IC>>(
    group, cfg);
  case Tp::FlowElement: return std::make_unique<JetLinkWriter<F,IC,FA>>(
    group, cfg, getFlowAssociations());
  case Tp::Electron: return std::make_unique<JetLinkWriter<E,IC,EA>>(
    group, cfg, getElectronAssociations());
  default: throw std::logic_error("unknown linked particle type");
  }
}
