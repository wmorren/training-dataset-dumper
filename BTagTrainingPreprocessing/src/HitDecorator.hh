#ifndef HIT_DECORATOR_HH
#define HIT_DECORATOR_HH

#include "AthContainers/AuxElement.h"

namespace xAOD {
  class Jet_v1;
  using Jet = Jet_v1;
  class Vertex_v1;
  using Vertex = Vertex_v1;
  class TrackMeasurementValidation_v1;
  using TrackMeasurementValidation = TrackMeasurementValidation_v1;
}

template <typename T>
using Acc = SG::AuxElement::ConstAccessor<T>;

class HitDecoratorConfig;

class HitDecorator
{
public:
  HitDecorator(const HitDecoratorConfig&);
  void decorate(const xAOD::Jet&,
                const std::vector<const xAOD::TrackMeasurementValidation*>&,
                const xAOD::Vertex&) const;

private:
  // you can add more decorators here. For things that aren't integers
  // you can create them with a template argument `float`.
  SG::AuxElement::Decorator<float> m_avg_hit_jet_0;
  SG::AuxElement::Decorator<float> m_avg_hit_jet_1;
  SG::AuxElement::Decorator<float> m_avg_hit_jet_2;
  SG::AuxElement::Decorator<float> m_avg_hit_jet_3;
  SG::AuxElement::Decorator<float> m_SOAP;
  SG::AuxElement::Decorator<float> m_multiJump03;
  SG::AuxElement::Decorator<int> m_nHits_L0;
  SG::AuxElement::Decorator<int> m_nHits_L1;
  SG::AuxElement::Decorator<int> m_nHits_L2;
  SG::AuxElement::Decorator<int> m_nHits_L3;
  SG::AuxElement::Decorator<int> m_total_hits;
  SG::AuxElement::Decorator<float> m_inv_sum;
  float m_dR_hit_to_jet;
  bool m_save_endcap_hits;
  bool m_use_splitProbability;
  Acc<int> m_layer;
  Acc<char> isSCT;
  Acc<int> m_bec;
  Acc<float> m_splitProbability1;
  Acc<float> m_splitProbability2;
};

#endif
